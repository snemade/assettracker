package anuvu.assettracker.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;

public class AssetUpdate {

    @Max(1)
    private int id  ;

    @Email(message = "email should be a valid email")
    private String newEpisodeName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNewEpisodeName() {
        return newEpisodeName;
    }

    public void setNewEpisodeName(String newEpisodeName) {
        this.newEpisodeName = newEpisodeName;
    }
}
