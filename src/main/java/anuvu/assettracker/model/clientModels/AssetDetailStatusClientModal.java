package anuvu.assettracker.model.clientModels;


import anuvu.assettracker.Entity.AssetDetail;

import java.util.Date;
import java.util.List;

public class AssetDetailStatusClientModal {

    private int asset_transid;
    public int getAsset_transid() {
        return asset_transid;
    }
    public void setAsset_transid(int asset_transid) {
        this.asset_transid = asset_transid;
    }

    private int asset_old_statusid;
    public int getAsset_old_statusid() {
        return asset_old_statusid;
    }
    public void setAsset_old_statusid(int asset_old_statusid) {
        this.asset_old_statusid = asset_old_statusid;
    }

    private int asset_new_statusid;
    public int getAsset_new_statusid() {
        return asset_new_statusid;
    }
    public void setAsset_new_statusid(int asset_new_statusid) {
        this.asset_new_statusid = asset_new_statusid;
    }

    private String task_start_time;
    public String getTask_start_time() {
        return task_start_time;
    }

    public void setTask_start_time(String task_start_time) {
        this.task_start_time = task_start_time;
    }

    private String task_end_time;
    public String getTask_end_time() {
        return task_end_time;
    }
    public void setTask_end_time(String task_end_time) {
        this.task_end_time = task_end_time;
    }

    private String notes;
    public String getNotes() {
        return notes;
    }
    public void setNotes(String notes) {
        this.notes = notes;
    }

    private String created_by;
    public String getCreated_by() {
        return created_by;
    }
    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

}
