//@Pattern(regexp="^[a-zA-Z0-9]{3}",message="length must be 3")
package anuvu.assettracker.model.clientModels;

import javax.validation.constraints.*;

public class AssetdetailClientmodel {

    private long trans_id;
    public long getTrans_id() {
        return trans_id;
    }
    public void setTrans_id(long trans_id) {
        this.trans_id = trans_id;
    }

    @Pattern(regexp="^[0-9]{6}",message="length must be 6 with format yyyymm")
    private String cycleID;
    public String getCycleID() {
        return cycleID;
    }
    public void setCycleID(String cycleID) {
        this.cycleID = cycleID;
    }

    // Distributor
    @NotNull(message = "Please provide distributor")
    @Min(value = 1, message = "Invalid Distributor")
    private Long distributorID;
    private String distributorName;

    public Long getDistributorID() {
        return distributorID;
    }
    public void setDistributorID(Long distributorID) {
        this.distributorID = distributorID;
    }
    public String getDistributorName() {
        return distributorName;
    }
    public void setDistributorName(String distributorName) {
        this.distributorName = distributorName;
    }

    @Size(min = 3)
    private String title;
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    // Asset Status
    @NotNull(message = "Please provide Status")
    @Min(value = 1, message = "Invalid Distributor")
    private Long statusID;
    private String statusName;

    public Long getStatusID() {
        return statusID;
    }
    public void setStatusID(Long statusID) {
        this.statusID = statusID;
    }
    public String getStatusName() {
        return statusName;
    }
    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    @NotNull
    @Size(min = 3, message = "Invalid User Name, should be more than 3.")
    private String created_by;
    public String getCreated_by() {
        return created_by;
    }
    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    // Airline
    private Long airlineID;
    private String airlineName;

    public Long getAirlineID() {
        return airlineID;
    }
    public void setAirlineID(Long airlineID) {
        this.airlineID = airlineID;
    }
    public String getAirlineName() {
        return airlineName;
    }
    public void setAirlineName(String airlineName) {
        this.airlineName = airlineName;
    }

    // Asset type
    private Long assettypeID;
    private String assetName;

    public Long getAssettypeID() {
        return assettypeID;
    }

    public void setAssettypeID(Long assettypeID) {
        this.assettypeID = assettypeID;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    private String order_date;
    public String getOrder_date() {
        return order_date;
    }
    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    private String po_number;
    public String getPo_number() {
        return po_number;
    }
    public void setPo_number(String po_number) {
        this.po_number = po_number;
    }

    private String episode_title;
    public String getEpisode_title() {
        return episode_title;
    }
    public void setEpisode_title(String episode_title) {
        this.episode_title = episode_title;
    }

    // Version
    private Long versionID;
    private String versionName;

    public Long getVersionID() {
        return versionID;
    }
    public void setVersionID(Long versionID) {
        this.versionID = versionID;
    }
    public String getVersionName() {
        return versionName;
    }
    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    private int season_no;
    public int getSeason_no() {
        return season_no;
    }
    public void setSeason_no(int season_no) {
        this.season_no = season_no;
    }

    private int episode_no;
    public int getEpisode_no() {
        return episode_no;
    }
    public void setEpisode_no(int episode_no) {
        this.episode_no = episode_no;
    }

    // Audio Language
    private Long audioLang;
    private String audioLangName;

    public Long getAudioLang() {
        return audioLang;
    }
    public void setAudioLang(Long audioLang) {
        this.audioLang = audioLang;
    }
    public String getAudioLangName() {
        return audioLangName;
    }
    public void setAudioLangName(String audioLangName) {
        this.audioLangName = audioLangName;
    }
    // Sub Language 1
    private Long subLang1;
    private String subLang1Name;

    public Long getSubLang1() {
        return subLang1;
    }
    public void setSubLang1(Long subLang1) {
        this.subLang1 = subLang1;
    }
    public String getSubLang1Name() {
        return subLang1Name;
    }
    public void setSubLang1Name(String subLang1Name) {
        this.subLang1Name = subLang1Name;
    }

    // Sub Language 2
    private Long subLang2;
    private String subLang2Name;

    public Long getSubLang2() {
        return subLang2;
    }
    public void setSubLang2(Long subLang2) {
        this.subLang2 = subLang2;
    }
    public String getSubLang2Name() {
        return subLang2Name;
    }
    public void setSubLang2Name(String subLang2Name) {
        this.subLang2Name = subLang2Name;
    }

    private String notes;
    public String getNotes() {
        return notes;
    }
    public void setNotes(String notes) {
        this.notes = notes;
    }

    private int isactive;
    public int getIsactive() {
        return isactive;
    }
    public void setIsactive(int isactive) {
        this.isactive = isactive;
    }

}
