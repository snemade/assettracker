package anuvu.assettracker.model.clientModels;

import javax.validation.constraints.Size;

public class DistributorClientmodel {

    private Long distributorid;

    @Size(min = 1)
    private String distributorName;

    public String getDistributorName() {
        return distributorName;
    }

    public Long getDistributorid() {
        return distributorid;
    }

    public void setDistributorid(Long distributorid) {
        this.distributorid = distributorid;
    }

    public void setDistributorName(String distributorName) {
        this.distributorName = distributorName;
    }
}
