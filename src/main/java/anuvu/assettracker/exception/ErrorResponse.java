package anuvu.assettracker.exception;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.validation.Errors;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@RequiredArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorResponse {
    private int status;
    private  String message;
    private String stackTrace;
    private List<ValidationError> errors;

    public String getStackTrace() {
        return stackTrace;
    }

    public void setStackTrace(String stackTrace) {
        this.stackTrace = stackTrace;
    }

    ErrorResponse(int _status, String _message){
        status = _status;
        message = _message;
    }
    @Getter
    @Setter
    @RequiredArgsConstructor
    private static class ValidationError {
        private String field;
        private String message;

        ValidationError(String _field, String _message){
            field = _field;
            message = _message;
        }
    }

    public void addValidationError(String field, String message){
        if(Objects.isNull(errors)){
            errors = new ArrayList<>();
        }
        errors.add(new ValidationError(field, message));
    }
}