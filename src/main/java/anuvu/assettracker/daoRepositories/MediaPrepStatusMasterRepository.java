package anuvu.assettracker.daoRepositories;

import anuvu.assettracker.Entity.MediaPrepStatusMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MediaPrepStatusMasterRepository extends JpaRepository<MediaPrepStatusMaster, Long> {
    List<MediaPrepStatusMaster> findAll();
}
