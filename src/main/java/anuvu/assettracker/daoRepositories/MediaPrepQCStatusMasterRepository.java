package anuvu.assettracker.daoRepositories;

import anuvu.assettracker.Entity.MediaPrepQCStatusMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MediaPrepQCStatusMasterRepository extends JpaRepository<MediaPrepQCStatusMaster, Long> {
    List<MediaPrepQCStatusMaster> findAll();
}
