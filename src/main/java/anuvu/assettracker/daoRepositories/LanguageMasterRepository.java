package anuvu.assettracker.daoRepositories;

import anuvu.assettracker.Entity.LanguageMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LanguageMasterRepository extends JpaRepository<LanguageMaster, Long> {
    List<LanguageMaster> findAll();
}
