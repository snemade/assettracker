package anuvu.assettracker.daoRepositories;

import anuvu.assettracker.Entity.UserDistributors;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserDistributorMapRepository extends JpaRepository<UserDistributors, Long> {
    List<UserDistributors> findAll();
}
