package anuvu.assettracker.daoRepositories;

import anuvu.assettracker.Entity.AssetDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AssetDetailRepository extends JpaRepository<AssetDetail, Long> {
    Optional<AssetDetail> findByTransidAndIsactive(long transid, int isactive);
//    Optional<List<AssetDetail>> findAllByTransidAndIsactive(long transid, int isactive);
//        Optional<AssetDetail> findByTransidAndIsactive(long transid, int isactive);
    Optional<List<AssetDetail>> findAllByIsactiveOrderByTransid(int isactive);

}
