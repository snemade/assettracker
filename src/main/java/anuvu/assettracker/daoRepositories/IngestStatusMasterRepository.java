package anuvu.assettracker.daoRepositories;

import anuvu.assettracker.Entity.IngestStatusMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IngestStatusMasterRepository extends JpaRepository<IngestStatusMaster, Long> {
    List<IngestStatusMaster> findAll();
}
