package anuvu.assettracker.daoRepositories;

import anuvu.assettracker.Entity.ComplianceEditingStatusMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ComplianceEditingStatusMasterRepository extends JpaRepository<ComplianceEditingStatusMaster, Long> {
    List<ComplianceEditingStatusMaster> findAll();
}
