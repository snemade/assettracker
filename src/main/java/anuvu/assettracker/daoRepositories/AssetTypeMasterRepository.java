package anuvu.assettracker.daoRepositories;

import anuvu.assettracker.Entity.AssetTypeMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AssetTypeMasterRepository extends JpaRepository<AssetTypeMaster, Long> {
    List<AssetTypeMaster> findAll();
}
