package anuvu.assettracker.daoRepositories;

import anuvu.assettracker.Entity.VersionMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VersionMasterRepository extends JpaRepository<VersionMaster, Long> {
    List<VersionMaster> findAll();
}
