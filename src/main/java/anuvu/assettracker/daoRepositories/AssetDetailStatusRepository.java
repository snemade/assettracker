package anuvu.assettracker.daoRepositories;

import anuvu.assettracker.Entity.AirlineMaster;
import anuvu.assettracker.Entity.AssetDetailStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AssetDetailStatusRepository extends JpaRepository<AssetDetailStatus, Long> {
    List<AssetDetailStatus> findAll();
}
