package anuvu.assettracker.daoRepositories;

import anuvu.assettracker.Entity.DeliveryQCMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DeliveryQCMasterRepository extends JpaRepository<DeliveryQCMaster, Long> {
    List<DeliveryQCMaster> findAll();
}
