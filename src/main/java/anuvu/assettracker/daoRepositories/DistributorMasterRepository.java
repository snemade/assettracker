//https://www.baeldung.com/spring-data-jpa-query
package anuvu.assettracker.daoRepositories;

import anuvu.assettracker.Entity.DistributorMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.NamedQuery;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface DistributorMasterRepository extends JpaRepository<DistributorMaster, Long> {
    List<DistributorMaster> findAll();

    @Transactional
    @Modifying
    @Query("update DistributorMaster a set  a.name = :#{#newDistName} WHERE a.distributorid = :#{#distID}")
    void updateDistributorName(@Param("distID") Long distID,
                          @Param("newDistName") String newDistName);
}
