package anuvu.assettracker.daoRepositories;

import anuvu.assettracker.Entity.AssetStatusMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AssetStatusMasterRepository extends JpaRepository<AssetStatusMaster, Long> {
    List<AssetStatusMaster> findAll();
}
