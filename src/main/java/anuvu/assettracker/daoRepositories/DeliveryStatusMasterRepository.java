package anuvu.assettracker.daoRepositories;

import anuvu.assettracker.Entity.DeliveryStatusMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DeliveryStatusMasterRepository extends JpaRepository<DeliveryStatusMaster, Long> {
    List<DeliveryStatusMaster> findAll();
}
