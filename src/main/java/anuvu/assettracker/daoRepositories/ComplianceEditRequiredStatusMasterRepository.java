package anuvu.assettracker.daoRepositories;

import anuvu.assettracker.Entity.ComplianceEditRequiredStatusMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ComplianceEditRequiredStatusMasterRepository extends JpaRepository<ComplianceEditRequiredStatusMaster, Long> {
    List<ComplianceEditRequiredStatusMaster> findAll();
}
