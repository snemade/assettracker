package anuvu.assettracker.daoRepositories;

import anuvu.assettracker.Entity.AirlineMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AirlineMasterRepository extends JpaRepository<AirlineMaster, Long> {
    List<AirlineMaster> findAll();
}
