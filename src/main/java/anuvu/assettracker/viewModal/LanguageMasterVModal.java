package anuvu.assettracker.viewModal;

import anuvu.assettracker.Entity.LanguageMaster;

import java.util.List;

public class LanguageMasterVModal {

    private List<LanguageMaster> languagemaster;

    public List<LanguageMaster> getLanguagemaster() {
        return languagemaster;
    }

    public void setLanguagemaster(List<LanguageMaster> languagemaster) {
        this.languagemaster = languagemaster;
    }
}
