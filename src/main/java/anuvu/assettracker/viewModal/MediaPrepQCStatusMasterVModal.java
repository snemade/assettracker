package anuvu.assettracker.viewModal;

import anuvu.assettracker.Entity.MediaPrepQCStatusMaster;

import java.util.List;

public class MediaPrepQCStatusMasterVModal {

    private List<MediaPrepQCStatusMaster> MediaPrepQCStatusmaster;

    public List<MediaPrepQCStatusMaster> getMediaPrepQCStatusmaster() {
        return MediaPrepQCStatusmaster;
    }

    public void setMediaPrepQCStatusmaster(List<MediaPrepQCStatusMaster> mediaPrepQCStatusmaster) {
        MediaPrepQCStatusmaster = mediaPrepQCStatusmaster;
    }
}
