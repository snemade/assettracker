package anuvu.assettracker.viewModal;

import anuvu.assettracker.Entity.DeliveryStatusMaster;

import java.util.List;

public class DeliveryStatusMasterVModal {

    private List<DeliveryStatusMaster> DeliveryStatusmaster;

    public List<DeliveryStatusMaster> getDeliveryStatusmaster() {
        return DeliveryStatusmaster;
    }

    public void setDeliveryStatusmaster(List<DeliveryStatusMaster> deliveryStatusmaster) {
        DeliveryStatusmaster = deliveryStatusmaster;
    }
}
