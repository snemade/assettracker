package anuvu.assettracker.viewModal;

import anuvu.assettracker.Entity.ComplianceEditRequiredStatusMaster;

import java.util.List;

public class ComplianceEditRequiredStatusMasterVModal {

    private List<ComplianceEditRequiredStatusMaster> ComplianceEditRequiredStatusmaster;

    public List<ComplianceEditRequiredStatusMaster> getComplianceEditRequiredStatusmaster() {
        return ComplianceEditRequiredStatusmaster;
    }

    public void setComplianceEditRequiredStatusmaster(List<ComplianceEditRequiredStatusMaster> complianceEditRequiredStatusmaster) {
        ComplianceEditRequiredStatusmaster = complianceEditRequiredStatusmaster;
    }
}
