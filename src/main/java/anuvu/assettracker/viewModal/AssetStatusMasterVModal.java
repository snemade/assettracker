package anuvu.assettracker.viewModal;

import anuvu.assettracker.Entity.AssetStatusMaster;

import java.util.List;

public class AssetStatusMasterVModal {

    private List<AssetStatusMaster> AssetStatusmaster;

    public List<AssetStatusMaster> getAssetStatusmaster() {
        return AssetStatusmaster;
    }

    public void setAssetStatusmaster(List<AssetStatusMaster> assetStatusmaster) {
        AssetStatusmaster = assetStatusmaster;
    }
}
