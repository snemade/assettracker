package anuvu.assettracker.viewModal;

import anuvu.assettracker.Entity.MediaPrepStatusMaster;

import java.util.List;

public class MediaPrepStatusMasterVModal {

    private List<MediaPrepStatusMaster> MediaPrepStatusmaster;

    public List<MediaPrepStatusMaster> getMediaPrepStatusmaster() {
        return MediaPrepStatusmaster;
    }

    public void setMediaPrepStatusmaster(List<MediaPrepStatusMaster> mediaPrepStatusmaster) {
        MediaPrepStatusmaster = mediaPrepStatusmaster;
    }
}
