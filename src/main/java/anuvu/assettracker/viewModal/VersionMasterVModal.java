package anuvu.assettracker.viewModal;

import anuvu.assettracker.Entity.VersionMaster;

import java.util.List;

public class VersionMasterVModal {

    private List<VersionMaster> versionemaster;

    public List<VersionMaster> getVersionemaster() {
        return versionemaster;
    }

    public void setVersionemaster(List<VersionMaster> versionemaster) {
        this.versionemaster = versionemaster;
    }
}
