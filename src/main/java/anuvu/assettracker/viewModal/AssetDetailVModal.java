package anuvu.assettracker.viewModal;


import anuvu.assettracker.Entity.AssetDetail;

import java.util.List;

public class AssetDetailVModal {

    private String userMessage;

    public String getUserMessage() {
        return userMessage;
    }

    public void setUserMessage(String userMessage) {
        this.userMessage = userMessage;
    }

    private List<AssetDetail> AssetDetails;

    public List<AssetDetail> getAssetDetails() {
        return AssetDetails;
    }

    public void setAssetDetails(List<AssetDetail> assetDetails) {
        AssetDetails = assetDetails;
    }
}
