package anuvu.assettracker.viewModal;

import anuvu.assettracker.Entity.AssetTypeMaster;

import java.util.List;

public class AssetTypeMasterVModal {
    public List<AssetTypeMaster> getAssetTypemaster() {
        return assetTypemaster;
    }

    public void setAssetTypemaster(List<AssetTypeMaster> assetTypemaster) {
        this.assetTypemaster = assetTypemaster;
    }

    private List<AssetTypeMaster> assetTypemaster;

}
