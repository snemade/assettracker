package anuvu.assettracker.viewModal;

import anuvu.assettracker.Entity.DeliveryQCMaster;

import java.util.List;

public class DeliveryQCMasterVModal {

    private List<DeliveryQCMaster> DeliveryQCmaster;

    public List<DeliveryQCMaster> getDeliveryQCmaster() {
        return DeliveryQCmaster;
    }

    public void setDeliveryQCmaster(List<DeliveryQCMaster> deliveryQCmaster) {
        DeliveryQCmaster = deliveryQCmaster;
    }
}
