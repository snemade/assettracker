package anuvu.assettracker.viewModal;

import anuvu.assettracker.Entity.IngestStatusMaster;

import java.util.List;

public class IngestStatusMasterVModal {

    private List<IngestStatusMaster> IngestStatusmaster;

    public List<IngestStatusMaster> getIngestStatusmaster() {
        return IngestStatusmaster;
    }

    public void setIngestStatusmaster(List<IngestStatusMaster> ingestStatusmaster) {
        IngestStatusmaster = ingestStatusmaster;
    }
}
