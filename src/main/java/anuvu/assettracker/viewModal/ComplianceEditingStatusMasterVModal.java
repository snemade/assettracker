package anuvu.assettracker.viewModal;

import anuvu.assettracker.Entity.ComplianceEditingStatusMaster;

import java.util.List;

public class ComplianceEditingStatusMasterVModal {

    private List<ComplianceEditingStatusMaster> ComplianceEditingStatusmaster;

    public List<ComplianceEditingStatusMaster> getComplianceEditingStatusmaster() {
        return ComplianceEditingStatusmaster;
    }

    public void setComplianceEditingStatusmaster(List<ComplianceEditingStatusMaster> complianceEditingStatusmaster) {
        ComplianceEditingStatusmaster = complianceEditingStatusmaster;
    }
}
