package anuvu.assettracker.viewModal;

import anuvu.assettracker.Entity.AirlineMaster;
import anuvu.assettracker.Entity.Asset;

import java.util.List;

public class AirlineMasterVModal {

    private List<AirlineMaster> airlinemasters;

    public List<AirlineMaster> getAirlinemasters() {
        return airlinemasters;
    }

    public void setAirlinemasters(List<AirlineMaster> airlinemasters) {
        this.airlinemasters = airlinemasters;
    }

}
