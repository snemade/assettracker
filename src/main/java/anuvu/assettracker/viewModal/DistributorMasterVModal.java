package anuvu.assettracker.viewModal;

import anuvu.assettracker.Entity.DistributorMaster;

import java.util.List;

public class DistributorMasterVModal {

    private List<DistributorMaster> Distributormaster;

    public List<DistributorMaster> getDistributormaster() {
        return Distributormaster;
    }

    public void setDistributormaster(List<DistributorMaster> distributormaster) {
        Distributormaster = distributormaster;
    }
}
