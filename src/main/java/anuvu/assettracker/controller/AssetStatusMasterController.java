package anuvu.assettracker.controller;

import anuvu.assettracker.daoRepositories.AssetStatusMasterRepository;
import anuvu.assettracker.viewModal.AssetStatusMasterVModal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class AssetStatusMasterController {

    @Autowired
    private AssetStatusMasterRepository repository;

    @CrossOrigin
    @GetMapping("/assetstatusmaster")
    public AssetStatusMasterVModal assetStatusmaster() {

        AssetStatusMasterVModal masterVModal = new AssetStatusMasterVModal();
        masterVModal.setAssetStatusmaster(repository.findAll());
        return masterVModal;
    }

}
