package anuvu.assettracker.controller;

import anuvu.assettracker.Entity.AirlineMaster;
import anuvu.assettracker.Entity.Asset;
import anuvu.assettracker.exception.ResourceNotFoundException;
import anuvu.assettracker.model.AssetUpdate;
import anuvu.assettracker.daoRepositories.AirlineMasterRepository;
import anuvu.assettracker.daoRepositories.AssetRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
public class AssetController {

    private static final Logger log = LogManager.getLogger(AssetController.class);

    @Autowired
    AssetRepository assetRepository;

    @Autowired
    private AirlineMasterRepository airlineMasterRepository;

    @CrossOrigin
    @GetMapping("/asset")
    public ResponseEntity<List<Asset>>  AssetAll() {

        List<Asset> assets = assetRepository.findAll();
        return new ResponseEntity<List<Asset>>(assets, HttpStatus.OK);

    }

    @Transactional
            (rollbackFor = Exception.class)
//      (rollbackFor = Exception.class, noRollbackFor = EntityNotFoundException.class)
    @CrossOrigin
    @PostMapping("/addAsset")
    public int  Create() {

        Asset asset = new Asset();
        asset.setEpisode_title("Mahabhart");

        AirlineMaster amRuntime = new AirlineMaster();
        amRuntime.setName("KLM");

        long airlineID = airlineMasterRepository.save(amRuntime).getId();
        log.info("AirlineMaster am .. ");
        //From Database
        Optional<AirlineMaster> am = airlineMasterRepository.findById((long) airlineID + 100) ;
        //asset.setAirline(am.get());

//        log.info("AirlineMaster am value - " + am.get());
        if(!am.isPresent()) {
            log.info("AirlineMaster am is null");
            throw new ResourceNotFoundException("Resource not found for id : " + airlineID);
        }
        log.info("AirlineMaster am is not null");
        if( 1 == 1)
            throw new RuntimeException();

        asset.setAirline(amRuntime);
        int idInserted = assetRepository.save(asset).getId();

        return  idInserted;
    }

    @CrossOrigin
    @PostMapping("/updateAsset")
    public int  Update(@Valid @RequestBody AssetUpdate assetUpdate) {

        Optional<Asset> asset = assetRepository.findById(assetUpdate.getId());
        asset.get().setEpisode_title(assetUpdate.getNewEpisodeName());
        assetRepository.save(asset.get());
        return  1;
    }

    @CrossOrigin
    @PostMapping("/deleteAsset")
    public int  Delete(int id) {

        Optional<Asset> asset = assetRepository.findById(id);
        assetRepository.delete(asset.get());
        return  1;
    }

}
