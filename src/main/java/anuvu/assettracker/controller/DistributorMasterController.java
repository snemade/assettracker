package anuvu.assettracker.controller;

import anuvu.assettracker.Entity.AssetDetail;
import anuvu.assettracker.Entity.DistributorMaster;
import anuvu.assettracker.daoRepositories.DistributorMasterRepository;
import anuvu.assettracker.model.clientModels.AssetdetailClientmodel;
import anuvu.assettracker.model.clientModels.DistributorClientmodel;
import anuvu.assettracker.viewModal.DistributorMasterVModal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

@RequestMapping("/distributormaster")
@RestController
public class DistributorMasterController {

    @PersistenceContext
    EntityManager em;

    @Autowired
    private DistributorMasterRepository distRepository;

    @CrossOrigin
    @GetMapping("/get")
    public DistributorMasterVModal distributormaster() {

        DistributorMasterVModal vModal = new DistributorMasterVModal();
        vModal.setDistributormaster(distRepository.findAll());
        return vModal;
    }
    @CrossOrigin
    @PostMapping("/create")
    public Long Create(@Valid @RequestBody DistributorClientmodel distributorClientmodel) {

        DistributorMaster distributorMaster = new DistributorMaster();
        distributorMaster.setName(distributorClientmodel.getDistributorName());
        distributorMaster.setIsactive(1);
        Long insertedID = distRepository.save(distributorMaster).getDistributorid();

        return insertedID;
    }

    @CrossOrigin
    @PostMapping("/remove")
    public Long Remove(@Valid @RequestBody DistributorClientmodel distributorClientmodel) {

        DistributorMaster distributorMaster = distRepository.getById(distributorClientmodel.getDistributorid());
        distributorMaster.setIsactive(0);
        Long insertedID = distRepository.save(distributorMaster).getDistributorid();

        return insertedID;
    }

    @CrossOrigin
    @PostMapping("/update")
    public Long Update(@Valid @RequestBody DistributorClientmodel distributorClientmodel) {

        DistributorMaster distributorMaster = distRepository.getById(distributorClientmodel.getDistributorid());

        distRepository.updateDistributorName(distributorMaster.getDistributorid() , distributorClientmodel.getDistributorName());

        return distributorClientmodel.getDistributorid();
    }
}
