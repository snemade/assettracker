package anuvu.assettracker.controller;

import anuvu.assettracker.daoRepositories.DeliveryQCMasterRepository;
import anuvu.assettracker.viewModal.DeliveryQCMasterVModal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class DeliveryQCMasterController {

    @Autowired
    private DeliveryQCMasterRepository repository;

    @CrossOrigin
    @GetMapping("/deliveryqcmaster")
    public DeliveryQCMasterVModal deliveryQCmaster() {

        DeliveryQCMasterVModal masterVModal = new DeliveryQCMasterVModal();
        masterVModal.setDeliveryQCmaster(repository.findAll());
        return masterVModal;
    }

}
