package anuvu.assettracker.controller;

import anuvu.assettracker.Entity.UserDistributors;
import anuvu.assettracker.daoRepositories.UserDistributorMapRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class UserDistributorMapController {

    @Autowired
    private UserDistributorMapRepository masterRepository;

    @CrossOrigin
    @GetMapping("/userdistributor")
    public List<UserDistributors> userdistributor() {

        //AirlineMasterVModal airlineMasterVModal = new AirlineMasterVModal();
        //airlineMasterVModal.setAirlinemasters(masterRepository.findAll());
        List<UserDistributors> userDistributors = masterRepository.findAll();
        return userDistributors;
    }

}
