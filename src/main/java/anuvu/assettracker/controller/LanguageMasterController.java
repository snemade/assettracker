package anuvu.assettracker.controller;

import anuvu.assettracker.daoRepositories.LanguageMasterRepository;
import anuvu.assettracker.viewModal.LanguageMasterVModal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class LanguageMasterController {

    @Autowired
    private LanguageMasterRepository repository;

    @CrossOrigin
    @GetMapping("/languagemaster")
    public LanguageMasterVModal languagemaster() {

        LanguageMasterVModal masterVModal = new LanguageMasterVModal();
        masterVModal.setLanguagemaster(repository.findAll());
        return masterVModal;
    }

}
