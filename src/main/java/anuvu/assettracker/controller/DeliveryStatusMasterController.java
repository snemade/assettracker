package anuvu.assettracker.controller;

import anuvu.assettracker.daoRepositories.DeliveryStatusMasterRepository;
import anuvu.assettracker.viewModal.DeliveryStatusMasterVModal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class DeliveryStatusMasterController {

    @Autowired
    private DeliveryStatusMasterRepository repository;

    @CrossOrigin
    @GetMapping("/deliverystatusmaster")
    public DeliveryStatusMasterVModal deliveryStatusmaster() {

        DeliveryStatusMasterVModal masterVModal = new DeliveryStatusMasterVModal();
        masterVModal.setDeliveryStatusmaster(repository.findAll());
        return masterVModal;
    }

}
