package anuvu.assettracker.controller;

import anuvu.assettracker.daoRepositories.AssetTypeMasterRepository;
import anuvu.assettracker.viewModal.AssetTypeMasterVModal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class AssetTypeMasterController {

    @Autowired
    private AssetTypeMasterRepository repository;

    @CrossOrigin
    @GetMapping("/assettypemaster")
    public AssetTypeMasterVModal assetTypemaster() {

        AssetTypeMasterVModal masterVModal = new AssetTypeMasterVModal();
        masterVModal.setAssetTypemaster(repository.findAll());
        return masterVModal;
    }

}
