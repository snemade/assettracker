package anuvu.assettracker.controller;

import anuvu.assettracker.Entity.AirlineMaster;
import anuvu.assettracker.Entity.AssetDetailStatus;
import anuvu.assettracker.daoRepositories.AirlineMasterRepository;
import anuvu.assettracker.daoRepositories.AssetDetailStatusRepository;
import anuvu.assettracker.model.clientModels.AssetDetailStatusClientModal;
import anuvu.assettracker.utility.Helper;
import anuvu.assettracker.viewModal.AirlineMasterVModal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


@RestController
public class AssetDetailStatusController {

    @Autowired
    private AssetDetailStatusRepository assetDetailStatusRepository;

    @CrossOrigin
    @PostMapping("assetDetailStatus/add")
    public long assetDetailStatus(@Valid @RequestBody AssetDetailStatusClientModal assetDetailStatusClientModal) throws ParseException {

        //SimpleDateFormat formatter =new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        AssetDetailStatus assetDetailStatus = new AssetDetailStatus();
        Date today = Calendar.getInstance().getTime();

        assetDetailStatus.setAsset_transid (assetDetailStatusClientModal.getAsset_transid());
        assetDetailStatus.setAsset_new_statusid(assetDetailStatusClientModal.getAsset_new_statusid());
//        assetDetailStatus.setTask_start_time(formatter.parse(assetDetailStatusClientModal.getTask_start_time()));
//        assetDetailStatus.setTask_end_time(formatter.parse(assetDetailStatusClientModal.getTask_end_time()));

        assetDetailStatus.setTask_start_time(Helper.getDateFromString("yyyy-MM-dd hh:mm:ss",assetDetailStatusClientModal.getTask_start_time()));
        assetDetailStatus.setTask_end_time(Helper.getDateFromString("yyyy-MM-dd hh:mm:ss",assetDetailStatusClientModal.getTask_end_time()));

        assetDetailStatus.setNotes(assetDetailStatusClientModal.getNotes());
        assetDetailStatus.setCreated_by(assetDetailStatusClientModal.getCreated_by());

        //assetDetailStatus.setCreate_date(formatter.parse(formatter.format(today)));
        assetDetailStatus.setCreate_date(Helper.getToday("yyyy-MM-dd hh:mm:ss"));

        assetDetailStatus.setIsactive(1);
        return assetDetailStatusRepository.save(assetDetailStatus).getId();

//        String sDate6 = "1998-12-16 13:30:50";
//        SimpleDateFormat formatter6=new SimpleDateFormat("yyyy-MM-ss hh:mm:ss");
//        Date date6=formatter6.parse(sDate6);
//        assetDetailStatus.setTask_end_time(date6);
    }

}
