package anuvu.assettracker.controller;

import anuvu.assettracker.daoRepositories.MediaPrepQCStatusMasterRepository;
import anuvu.assettracker.viewModal.MediaPrepQCStatusMasterVModal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class MediaPrepQCStatusMasterController {

    @Autowired
    private MediaPrepQCStatusMasterRepository repository;

    @CrossOrigin
    @GetMapping("/mediaprepqcstatusmaster")
    public MediaPrepQCStatusMasterVModal MediaPrepQCStatusmaster() {

        MediaPrepQCStatusMasterVModal masterVModal = new MediaPrepQCStatusMasterVModal();
        masterVModal.setMediaPrepQCStatusmaster(repository.findAll());
        return masterVModal;
    }

}
