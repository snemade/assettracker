package anuvu.assettracker.controller;

import anuvu.assettracker.Entity.AirlineMaster;
import anuvu.assettracker.daoRepositories.AirlineMasterRepository;
import anuvu.assettracker.viewModal.AirlineMasterVModal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class AirlineMasterController {

    @Autowired
    private AirlineMasterRepository airlineMasterRepository;

    @CrossOrigin
    @GetMapping("/airlinemaster/get")
    public AirlineMasterVModal getAirlinemaster() {

        AirlineMasterVModal airlineMasterVModal = new AirlineMasterVModal();
        List<AirlineMaster> airlines = airlineMasterRepository.findAll();

        /* Explicit call to fill data into lazy asset list
        for (AirlineMaster am :airlines
             ) {
            am.getAssets();
        }
        */

        airlineMasterVModal.setAirlinemasters(airlines);
        return airlineMasterVModal; //assetList will not be loaded since it is lazy loaded, if we call explicit airlineMasterVModal.getAirlinemasters().get(0).getAssets().get(1) then asset table query will be executed

    }

    @CrossOrigin
    @PostMapping("airlinemaster/add")
    public long addAirlinemaster(String name) {

        AirlineMaster am = new AirlineMaster();
        am.setName(name);
        return airlineMasterRepository.save(am).getId();
    }

}
