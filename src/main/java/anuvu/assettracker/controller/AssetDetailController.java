package anuvu.assettracker.controller;

import anuvu.assettracker.Entity.*;
import anuvu.assettracker.daoRepositories.*;
import anuvu.assettracker.model.AppResponseEntity;
import anuvu.assettracker.model.clientModels.AssetdetailClientmodel;
import anuvu.assettracker.viewModal.AssetDetailVModal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.Valid;
import java.util.*;

@RequestMapping("/Assetdetail")
@RestController
public class AssetDetailController {

    @Autowired
    private AssetDetailRepository assetDetailRepository;

    @Autowired
    private DistributorMasterRepository distributorMasterRepository;

    @Autowired
    private AirlineMasterRepository airlineMasterRepository;

    @Autowired
    private AssetTypeMasterRepository assetTypeMasterRepository;

    @Autowired
    private VersionMasterRepository versionMasterRepository;

    @Autowired
    private LanguageMasterRepository aodlanguageMasterRepository;

    @Autowired
    private LanguageMasterRepository sub1languageMasterRepository;

    @Autowired
    private LanguageMasterRepository sub2languageMasterRepository;

    @Autowired
    private AssetStatusMasterRepository assetStatusMasterRepository;

    @ManyToOne(optional = false)
    @JoinColumn(name = "")
    private DistributorMaster distributorMaster;
    //private static final LogManager logManager = LogManager.getLogger(AssetDetailController.class);

    @CrossOrigin
    @GetMapping("/get/all")
    public ResponseEntity<AssetDetailVModal> AssetDetail() {
        AssetDetailVModal masterVModal = new AssetDetailVModal();
        //masterVModal.setAssetDetails(assetDetailRepository.findAll());
        Optional<List<AssetDetail>> assetDetail = assetDetailRepository.findAllByIsactiveOrderByTransid(1);
        if(assetDetail.isPresent() && assetDetail.get().size()>0){
            masterVModal.setAssetDetails(assetDetail.get());
            masterVModal.setUserMessage("Total "+ assetDetail.get().size() + " assets found");
            return new ResponseEntity<AssetDetailVModal>(masterVModal, HttpStatus.OK);
        }
        else{
            masterVModal.setUserMessage("Asset not found");
            return new ResponseEntity<AssetDetailVModal>(masterVModal, HttpStatus.NOT_FOUND);
        }

    }

    @CrossOrigin
    @GetMapping("/get")
    public ResponseEntity<AssetDetailVModal>  AssetDetail(@RequestParam(name = "assetID") Long assetID) {

        AssetDetailVModal masterVModal = new AssetDetailVModal();
        //Optional<AssetDetail> assetDetail = assetDetailRepository.findById(assetID);
        Optional<AssetDetail> assetDetail = assetDetailRepository.findByTransidAndIsactive(assetID,1);
        //Optional<AssetDetail> assetDetail = assetDetailRepository.findByTransidAndIsactive(assetID,1);

        if(assetDetail.isPresent()){
            List<AssetDetail> asset = new ArrayList<AssetDetail>();
            asset.add(assetDetail.get());
            masterVModal.setUserMessage("Asset found.");
            masterVModal.setAssetDetails(asset);
            return new ResponseEntity<AssetDetailVModal>(masterVModal, HttpStatus.OK);
        }
        else{
            masterVModal.setUserMessage("Asset not found.");
            return new ResponseEntity<AssetDetailVModal>(masterVModal, HttpStatus.NOT_FOUND);
        }

    }

    @CrossOrigin
    @PostMapping("/create")
    public Long Create(@Valid @RequestBody AssetdetailClientmodel assetdetailClientmodel) {

        AssetDetail assetDetail = new AssetDetail();
        assetDetail.setCycle_id(assetdetailClientmodel.getCycleID());
        // Set Distributor
        if(assetdetailClientmodel.getDistributorID() != null && assetdetailClientmodel.getDistributorID()>0){
             DistributorMaster distributorMaster = distributorMasterRepository.getById(assetdetailClientmodel.getDistributorID());
            if(distributorMaster.getName().trim().length() >0){
                distributorMaster.setIsactive(1);
                assetDetail.setDistributorMaster(distributorMaster);
            }
        }

        // Set Airline
        if(assetdetailClientmodel.getAirlineID() != null && assetdetailClientmodel.getAirlineID()>0){
            AirlineMaster airlinemaster = airlineMasterRepository.getById(assetdetailClientmodel.getAirlineID());
            if(airlinemaster.getName().trim().length() >0){
                airlinemaster.setIsactive(1);
                assetDetail.setAirlineMaster(airlinemaster);
            }
        }

        // Set Asset Type
        if(assetdetailClientmodel.getAssettypeID() != null && assetdetailClientmodel.getAssettypeID()>0){
            AssetTypeMaster assetTypeMaster = assetTypeMasterRepository.getById(assetdetailClientmodel.getAssettypeID());
            if(assetTypeMaster.getName().trim().length() >0){
                assetTypeMaster.setIsactive(1);
                assetDetail.setAssetTypeMaster(assetTypeMaster);
            }
        }

        //  Order date
        assetDetail.setOrder_date(assetdetailClientmodel.getOrder_date());
        //  PO No
        assetDetail.setPo_number(assetdetailClientmodel.getPo_number());
        //  title
        assetDetail.setTitle(assetdetailClientmodel.getTitle());
        //  episode_title
        assetDetail.setEpisode_title(assetdetailClientmodel.getEpisode_title());
        //  season_no
        assetDetail.setSeason_no(assetdetailClientmodel.getSeason_no());
        //  episode no
        assetDetail.setEpisode_no(assetdetailClientmodel.getEpisode_no());
        //  notes
        assetDetail.setNotes(assetdetailClientmodel.getNotes());

        // Set Version
        if(assetdetailClientmodel.getVersionID() != null && assetdetailClientmodel.getVersionID()>0){
            VersionMaster versionMaster = versionMasterRepository.getById(assetdetailClientmodel.getVersionID());
            if(versionMaster.getName().trim().length() >0){
                versionMaster.setIsactive(1);
                assetDetail.setVersionMaster(versionMaster);
            }
        }

        // Set Audio Lang
        if(assetdetailClientmodel.getAudioLang() != null && assetdetailClientmodel.getAudioLang()>0){
            LanguageMaster aodLanguageMaster = aodlanguageMasterRepository.getById(assetdetailClientmodel.getAudioLang());
            if(aodLanguageMaster.getName().trim().length() >0){
                aodLanguageMaster.setIsActive(1);
                assetDetail.setAudioLanguageMaster(aodLanguageMaster);
            }
        }

        // Set Sub Lang 1
        if(assetdetailClientmodel.getSubLang1() != null && assetdetailClientmodel.getSubLang1()>0){
            LanguageMaster sub1LanguageMaster = sub1languageMasterRepository.getById(assetdetailClientmodel.getSubLang1());
            if(sub1LanguageMaster.getName().trim().length() >0){
                sub1LanguageMaster.setIsActive(1);
                assetDetail.setSub1LanguageMaster(sub1LanguageMaster);
            }
        }

        // Set Sub Lang 2
        if(assetdetailClientmodel.getSubLang2() != null && assetdetailClientmodel.getSubLang2()>0){
            LanguageMaster sub2LanguageMaster = sub2languageMasterRepository.getById(assetdetailClientmodel.getSubLang2());
            if(sub2LanguageMaster.getName().trim().length() >0){
                sub2LanguageMaster.setIsActive(1);
                assetDetail.setSub2LanguageMaster(sub2LanguageMaster);
            }
        }

        // Set Status
        if(assetdetailClientmodel.getStatusID() != null && assetdetailClientmodel.getStatusID()>0){
            AssetStatusMaster assetStatusMaster = assetStatusMasterRepository.getById(assetdetailClientmodel.getStatusID());
            if(assetStatusMaster.getName().trim().length() >0){
                assetStatusMaster.setIsactive(1);
                assetDetail.setAssetStatusMaster(assetStatusMaster);
            }
        }

        //  CreateBY
        assetDetail.setCreated_by(assetdetailClientmodel.getCreated_by());
        //  is Active
        assetDetail.setIsactive(1);

        Long insertedID = assetDetailRepository.save(assetDetail).getTransid();
        return insertedID;
    }

    @CrossOrigin
    @PostMapping("/update")
    public ResponseEntity<AppResponseEntity> Update(@Valid @RequestBody AssetdetailClientmodel assetdetailClientmodel) {
//
//        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
//        Validator validator = validatorFactory.usingContext().getValidator();
//
//        Set constrains = validator.validate(assetdetailClientmodel);

        AppResponseEntity appResponseEntity = new AppResponseEntity();
        String fieldsUpdated = "";
        if(assetDetailRepository.findById(assetdetailClientmodel.getTrans_id()).isPresent()){

            // Prepare assetDetail Record for update
            AssetDetail assetDetail = assetDetailRepository.getById(assetdetailClientmodel.getTrans_id());
            //Check and update cycle id
            if(!assetDetail.getCycle_id().trim().equals(assetdetailClientmodel.getCycleID().trim()) ){
                assetDetail.setCycle_id(assetdetailClientmodel.getCycleID());
                fieldsUpdated = ", CycleID";
            }
            //Check and Update Distributor ID
            if(assetdetailClientmodel.getDistributorID() != assetDetail.getDistributorMaster().getDistributorid()){
                if(distributorMasterRepository.findById(assetdetailClientmodel.getDistributorID()).isPresent()){
                    DistributorMaster distributorMaster = distributorMasterRepository.getById(assetdetailClientmodel.getDistributorID());
                    assetDetail.setDistributorMaster(distributorMaster);
                    fieldsUpdated = fieldsUpdated + ", Distributor";
                }
            }

            //Check and Update Airline ID
            if(Objects.requireNonNullElse(assetdetailClientmodel.getAirlineID(), 0L) == 0 ){
                assetDetail.setAirlineMaster(null);
                fieldsUpdated = fieldsUpdated + ", airline";
            }
            else if((assetDetail.getAirlineMaster() == null && Objects.requireNonNullElse(assetdetailClientmodel.getAirlineID(), 0L)>0 )
                || ( assetDetail.getAirlineMaster() != null && assetdetailClientmodel.getAirlineID() != null &&
                        Objects.requireNonNullElse(assetdetailClientmodel.getAirlineID(), 0L) > 0 &&
                        assetdetailClientmodel.getAirlineID() != assetDetail.getAirlineMaster().getId())

            ){
                if(airlineMasterRepository.findById(assetdetailClientmodel.getDistributorID()).isPresent()){
                    AirlineMaster airlineMaster = airlineMasterRepository.getById(assetdetailClientmodel.getAirlineID());
                    assetDetail.setAirlineMaster(airlineMaster);
                    fieldsUpdated = fieldsUpdated + ", airline";
                }
            }

            //Check and Update Asset type ID
            if(Objects.requireNonNullElse(assetdetailClientmodel.getAssettypeID(), 0L) == 0 ){
                assetDetail.setAssetTypeMaster(null);
                fieldsUpdated = fieldsUpdated + ", asset type";
            }
            else
            if((assetDetail.getAssetTypeMaster() == null && Objects.requireNonNullElse(assetdetailClientmodel.getAssettypeID(), 0L)>0 )
                    || ( assetDetail.getAssetTypeMaster() != null && assetdetailClientmodel.getAssettypeID() != null &&
                    Objects.requireNonNullElse(assetdetailClientmodel.getAssettypeID(), 0L) > 0 &&
                    assetdetailClientmodel.getAssettypeID() != assetDetail.getAssetTypeMaster().getId())
            ){

                if(assetTypeMasterRepository.findById(assetdetailClientmodel.getAssettypeID()).isPresent()){
                    AssetTypeMaster assetTypeMaster = assetTypeMasterRepository.getById(assetdetailClientmodel.getAssettypeID());
                    assetDetail.setAssetTypeMaster(assetTypeMaster);
                    fieldsUpdated = fieldsUpdated + ", asset type";
                }
            }

            //  Order date
//            if(!assetDetail.getOrder_date().trim().equals(assetdetailClientmodel.getOrder_date().trim())
            if (    Objects.requireNonNullElse(assetdetailClientmodel.getOrder_date(), "") != Objects.requireNonNullElse(assetDetail.getOrder_date(), "")
            ){
                assetDetail.setOrder_date(assetdetailClientmodel.getOrder_date());
                fieldsUpdated = fieldsUpdated +", Order Date";
            }

            //  PO No
//            if(!assetDetail.getPo_number().trim().equals(assetdetailClientmodel.getPo_number().trim()) ){
            if (    Objects.requireNonNullElse(assetdetailClientmodel.getPo_number(), "") != Objects.requireNonNullElse(assetDetail.getPo_number(), "")
            ){
                assetDetail.setPo_number(assetdetailClientmodel.getPo_number());
                fieldsUpdated = fieldsUpdated +", PO No";
            }

            //  title
            //if(!assetDetail.getTitle().trim().equals(assetdetailClientmodel.getTitle().trim()) ){
            if (    Objects.requireNonNullElse(assetdetailClientmodel.getTitle(), "") != Objects.requireNonNullElse(assetDetail.getTitle(), "")
            ){
                assetDetail.setTitle(assetdetailClientmodel.getTitle());
                fieldsUpdated = fieldsUpdated +", Title";
            }

            //  episode_title
//            if(!assetDetail.getEpisode_title().trim().equals(assetdetailClientmodel.getEpisode_title().trim()) ){
            if (    Objects.requireNonNullElse(assetdetailClientmodel.getEpisode_title(), "") != Objects.requireNonNullElse(assetDetail.getEpisode_title(), "")
            ){
                assetDetail.setEpisode_title(assetdetailClientmodel.getEpisode_title());
                fieldsUpdated = fieldsUpdated +", Episode Title";
            }
            //  season_no
//            if(assetDetail.getSeason_no() != assetdetailClientmodel.getSeason_no() ){
            if (    Objects.requireNonNullElse(assetdetailClientmodel.getSeason_no(), "") != Objects.requireNonNullElse(assetDetail.getSeason_no(), "")
            ){
                assetDetail.setSeason_no(assetdetailClientmodel.getSeason_no());
                fieldsUpdated = fieldsUpdated +", Season no";
            }

            //  episode no
//            if(assetDetail.getEpisode_no() != assetdetailClientmodel.getEpisode_no() ){
            if (    Objects.requireNonNullElse(assetdetailClientmodel.getEpisode_no(), "") != Objects.requireNonNullElse(assetDetail.getEpisode_no(), "")
            ){
                assetDetail.setEpisode_no(assetdetailClientmodel.getEpisode_no());
                fieldsUpdated = fieldsUpdated +", Episode no";
            }

            //Check and Update Version
            //if(assetDetail.getVersionMaster() != null && (assetdetailClientmodel.getVersionID() != assetDetail.getVersionMaster().getId())
            if(Objects.requireNonNullElse(assetdetailClientmodel.getVersionID(), 0L) == 0 ){
                assetDetail.setVersionMaster(null);
                fieldsUpdated = fieldsUpdated + ", version";
            }
            else
            if((assetDetail.getVersionMaster() == null && Objects.requireNonNullElse(assetdetailClientmodel.getVersionID(), 0L)>0 )
                    || ( assetDetail.getVersionMaster() != null && assetdetailClientmodel.getVersionID() != null &&
                    Objects.requireNonNullElse(assetdetailClientmodel.getVersionID(), 0L) > 0 &&
                    assetdetailClientmodel.getVersionID() != assetDetail.getVersionMaster().getId())
            ){
                if(versionMasterRepository.findById(assetdetailClientmodel.getVersionID()).isPresent()){
                    VersionMaster versionMaster = versionMasterRepository.getById(assetdetailClientmodel.getVersionID());
                    assetDetail.setVersionMaster(versionMaster);
                    fieldsUpdated = fieldsUpdated + ", Version";
                }
            }

            //Check and Update AOD lang
            //if(assetDetail.getAudioLanguageMaster() != null && (assetdetailClientmodel.getAudioLang() != assetDetail.getAudioLanguageMaster().getId())){
            if(Objects.requireNonNullElse(assetdetailClientmodel.getAudioLang(), 0L) == 0 ){
                assetDetail.setAudioLanguageMaster(null);
                fieldsUpdated = fieldsUpdated + ", AOD language";
            }
            else
            if((assetDetail.getAudioLanguageMaster() == null && Objects.requireNonNullElse(assetdetailClientmodel.getAudioLang(), 0L)>0 )
                    || ( assetDetail.getAudioLanguageMaster() != null && assetdetailClientmodel.getAudioLang() != null &&
                    Objects.requireNonNullElse(assetdetailClientmodel.getAudioLang(), 0L) > 0 &&
                    assetdetailClientmodel.getAudioLang() != assetDetail.getAudioLanguageMaster().getId())
            ){
                if(aodlanguageMasterRepository.findById(assetdetailClientmodel.getAudioLang()).isPresent()){
                    LanguageMaster languageMaster = aodlanguageMasterRepository.getById(assetdetailClientmodel.getAudioLang());
                    assetDetail.setAudioLanguageMaster(languageMaster);
                    fieldsUpdated = fieldsUpdated + ", AOD language";
                }
            }

            //Check and Update SUB lang
//            if(assetDetail.getSub1LanguageMaster() != null && (assetdetailClientmodel.getSubLang1() != assetDetail.getSub1LanguageMaster().getId())){
            if(Objects.requireNonNullElse(assetdetailClientmodel.getSubLang1(), 0L) == 0 ){
                assetDetail.setSub1LanguageMaster(null);
                fieldsUpdated = fieldsUpdated + ", Sub1 language";
            }
            else
            if((assetDetail.getSub1LanguageMaster() == null && Objects.requireNonNullElse(assetdetailClientmodel.getSubLang1(), 0L)>0 )
                    || ( assetDetail.getSub1LanguageMaster() != null && assetdetailClientmodel.getSubLang1() != null &&
                    Objects.requireNonNullElse(assetdetailClientmodel.getSubLang1(), 0L) > 0 &&
                    assetdetailClientmodel.getSubLang1() != assetDetail.getSub1LanguageMaster().getId())
            ){
                if(sub1languageMasterRepository.findById(assetdetailClientmodel.getSubLang1()).isPresent()){
                    LanguageMaster languageMaster = sub1languageMasterRepository.getById(assetdetailClientmodel.getSubLang1());
                    assetDetail.setSub1LanguageMaster(languageMaster);
                    fieldsUpdated = fieldsUpdated + ", Sub language 1";
                }
            }

            //Check and Update AOD lang
//            if(assetDetail.getSub2LanguageMaster() != null && (assetdetailClientmodel.getSubLang2() != assetDetail.getSub2LanguageMaster().getId())){
            if(Objects.requireNonNullElse(assetdetailClientmodel.getSubLang2(), 0L) == 0 ){
                assetDetail.setSub2LanguageMaster(null);
                fieldsUpdated = fieldsUpdated + ", Sub2 language";
            }
            else
            if((assetDetail.getSub2LanguageMaster() == null && Objects.requireNonNullElse(assetdetailClientmodel.getSubLang2(), 0L)>0 )
                    || ( assetDetail.getSub2LanguageMaster() != null && assetdetailClientmodel.getSubLang2() != null &&
                    Objects.requireNonNullElse(assetdetailClientmodel.getSubLang2(), 0L) > 0 &&
                    assetdetailClientmodel.getSubLang2() != assetDetail.getSub2LanguageMaster().getId())
            ){
                if(sub2languageMasterRepository.findById(assetdetailClientmodel.getSubLang2()).isPresent()){
                    LanguageMaster languageMaster = sub2languageMasterRepository.getById(assetdetailClientmodel.getSubLang2());
                    assetDetail.setSub2LanguageMaster(languageMaster);
                    fieldsUpdated = fieldsUpdated + ", Sub language 2";
                }
            }

            //Check and Update Status Master
            //if(assetDetail.getAssetStatusMaster() != null && (assetdetailClientmodel.getStatusID() != assetDetail.getAssetStatusMaster().getId())){
            if((assetDetail.getAssetStatusMaster() == null && Objects.requireNonNullElse(assetdetailClientmodel.getStatusID(), 0L)>0 )
                    || ( assetDetail.getAssetStatusMaster() != null && assetdetailClientmodel.getStatusID() != null &&
                    Objects.requireNonNullElse(assetdetailClientmodel.getStatusID(), 0L) > 0 &&
                    assetdetailClientmodel.getStatusID() != assetDetail.getAssetStatusMaster().getId())
            ){
                if(assetStatusMasterRepository.findById(assetdetailClientmodel.getStatusID()).isPresent()){
                    AssetStatusMaster statusMaster = assetStatusMasterRepository.getById(assetdetailClientmodel.getStatusID());
                    assetDetail.setAssetStatusMaster(statusMaster);
                    fieldsUpdated = fieldsUpdated + ", Status";
                }
            }
            //  notes
//            if(!assetDetail.getNotes().trim().equals(assetdetailClientmodel.getNotes().trim()) ){
            if (    Objects.requireNonNullElse(assetdetailClientmodel.getNotes(), "") != Objects.requireNonNullElse(assetDetail.getNotes(), "")
            ){

                assetDetail.setNotes(assetdetailClientmodel.getNotes());
                fieldsUpdated = fieldsUpdated +", Notes";
            }

            assetDetailRepository.save(assetDetail);
            if(fieldsUpdated.trim().length() == 0){

                appResponseEntity.setUserMessage("No Asset details updated, please contact administrator.");
            }
            else{

                appResponseEntity.setUserMessage("Asset details "
                        + (fieldsUpdated.length()>0 ? fieldsUpdated.substring(1,fieldsUpdated.length()).trim() : "")
                        +  " updated.");
            }
            appResponseEntity.setStatusCode(HttpStatus.OK.value());
            return new ResponseEntity<AppResponseEntity>(appResponseEntity, HttpStatus.OK);
        }
        else {
            appResponseEntity.setUserMessage("Asset detail Trans ID "+ assetdetailClientmodel.getTrans_id() + " not found");
            appResponseEntity.setStatusCode(HttpStatus.NOT_FOUND.value());
            return new ResponseEntity<AppResponseEntity>(appResponseEntity, HttpStatus.NOT_FOUND);
        }
    }

    @CrossOrigin
    @PostMapping("/delete")
    public ResponseEntity<AppResponseEntity> RemoveAssetDetail(@RequestParam(name = "transid") Long transid) {

        AppResponseEntity appResponseEntity = new AppResponseEntity();
        String fieldsUpdated = "";
        if(assetDetailRepository.findById(transid).isPresent()) {
            AssetDetailVModal masterVModal = new AssetDetailVModal();
        Optional<AssetDetail> assetDetail = assetDetailRepository.findById(transid);
        if(assetDetail.isPresent()){
            AssetDetail assetDtl = assetDetail.get();
            assetDtl.setIsactive(0);
            Long insertedID = assetDetailRepository.save(assetDtl).getTransid();
            appResponseEntity.setUserMessage("Asset details "
                    + (Objects.requireNonNullElse(insertedID, 0L)>0 ? "Entry is removed with details, " + " transaction id " + assetDtl.getTransid() + ", Tilte - " + assetDtl.getTitle() + " " + assetDtl.getEpisode_title() : "Error while removing entry.")
                    );
        }
        }

        return new ResponseEntity<AppResponseEntity>(appResponseEntity, HttpStatus.OK);
    }

}
