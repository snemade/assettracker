package anuvu.assettracker.controller;

import anuvu.assettracker.daoRepositories.MediaPrepStatusMasterRepository;
import anuvu.assettracker.viewModal.MediaPrepStatusMasterVModal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class MediaPrepStatusMasterController {

    @Autowired
    private MediaPrepStatusMasterRepository repository;

    @CrossOrigin
    @GetMapping("/mediaprepstatusmaster")
    public MediaPrepStatusMasterVModal airlinemaster() {

        MediaPrepStatusMasterVModal masterVModal = new MediaPrepStatusMasterVModal();
        masterVModal.setMediaPrepStatusmaster(repository.findAll());
        return masterVModal;
    }

}
