package anuvu.assettracker.controller;

import anuvu.assettracker.daoRepositories.ComplianceEditRequiredStatusMasterRepository;
import anuvu.assettracker.viewModal.ComplianceEditRequiredStatusMasterVModal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class ComplianceEditRequiredStatusMasterController {

    @Autowired
    private ComplianceEditRequiredStatusMasterRepository repository;

    @CrossOrigin
    @GetMapping("/complianceeditrequiredstatusmaster")
    public ComplianceEditRequiredStatusMasterVModal ComplianceEditRequiredStatusmaster() {

        ComplianceEditRequiredStatusMasterVModal masterVModal = new ComplianceEditRequiredStatusMasterVModal();
        masterVModal.setComplianceEditRequiredStatusmaster(repository.findAll());
        return masterVModal;
    }

}
