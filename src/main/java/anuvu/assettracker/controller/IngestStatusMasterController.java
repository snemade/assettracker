package anuvu.assettracker.controller;

import anuvu.assettracker.daoRepositories.IngestStatusMasterRepository;
import anuvu.assettracker.viewModal.IngestStatusMasterVModal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class IngestStatusMasterController {

    @Autowired
    private IngestStatusMasterRepository repository;

    @CrossOrigin
    @GetMapping("/ingeststatusmaster")
    public IngestStatusMasterVModal ingestStatusmaster() {

        IngestStatusMasterVModal masterVModal = new IngestStatusMasterVModal();
        masterVModal.setIngestStatusmaster(repository.findAll());
        return masterVModal;
    }

}
