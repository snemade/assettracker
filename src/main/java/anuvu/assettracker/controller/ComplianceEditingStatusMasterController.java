package anuvu.assettracker.controller;

import anuvu.assettracker.daoRepositories.ComplianceEditingStatusMasterRepository;
import anuvu.assettracker.viewModal.ComplianceEditingStatusMasterVModal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class ComplianceEditingStatusMasterController {

    @Autowired
    private ComplianceEditingStatusMasterRepository repository;

    @CrossOrigin
    @GetMapping("/complianceeditingstatusmaster")
    public ComplianceEditingStatusMasterVModal ComplianceEditingStatusmaster() {

        ComplianceEditingStatusMasterVModal masterVModal = new ComplianceEditingStatusMasterVModal();
        masterVModal.setComplianceEditingStatusmaster(repository.findAll());
        return masterVModal;
    }

}
