package anuvu.assettracker.controller;

import anuvu.assettracker.daoRepositories.VersionMasterRepository;
import anuvu.assettracker.viewModal.VersionMasterVModal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class VersionMasterController {

    @Autowired
    private VersionMasterRepository repository;

    @CrossOrigin
    @GetMapping("/versionmaster")
    public VersionMasterVModal versionmaster() {

        VersionMasterVModal masterVModal = new VersionMasterVModal();
        masterVModal.setVersionemaster(repository.findAll());
        return masterVModal;
    }

}
