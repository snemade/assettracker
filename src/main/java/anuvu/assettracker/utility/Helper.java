package anuvu.assettracker.utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Helper {

    public static Date getToday(String format) throws ParseException {

        SimpleDateFormat formatter =new SimpleDateFormat(format);
        Date today = Calendar.getInstance().getTime();
        return formatter.parse(formatter.format(today));
    }

    public static Date getDateFromString(String format,String inputStringDate) throws ParseException {

        SimpleDateFormat formatter =new SimpleDateFormat(format);
        return formatter.parse(inputStringDate);
    }

}
