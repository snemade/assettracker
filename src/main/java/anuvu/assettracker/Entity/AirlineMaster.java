package anuvu.assettracker.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonView;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "Airline",
        uniqueConstraints=
        @UniqueConstraint(columnNames={"name", "isactive"}))
public class AirlineMaster {
    @Id
    @SequenceGenerator(name="jpaPkSeq", sequenceName="JPA_PK_SEQ", allocationSize=1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    private String name;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @JsonManagedReference // It will avoid recursive loop and stack overflow error. Loop -AirlineMaster contains List<Asset> and each Asset contains AirlineMaster
    @JsonIgnore
    @OneToMany(mappedBy = "airlineMaster", fetch = FetchType.LAZY) //object name in assetDetail - airlineMaster
    private List<AssetDetail> assetdetails;

    private int isactive;
    public int getIsactive() {
        return isactive;
    }
    public void setIsactive(int isactive) {
        this.isactive = isactive;
    }

}
