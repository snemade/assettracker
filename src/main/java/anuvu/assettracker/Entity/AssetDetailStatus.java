package anuvu.assettracker.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.validator.internal.constraintvalidators.bv.time.pastorpresent.PastOrPresentValidatorForLocalDateTime;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "asset_detail_status")
public class AssetDetailStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "asset_detail_status_seq")
    @SequenceGenerator(name="asset_detail_status_seq", sequenceName = "asset_detail_status_seq", allocationSize=1, initialValue = 1)
    private long id;
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    private int asset_transid;
    public int getAsset_transid() {
        return asset_transid;
    }
    public void setAsset_transid(int asset_transid) {
        this.asset_transid = asset_transid;
    }

    private int asset_old_statusid;
    public int getAsset_old_statusid() {
        return asset_old_statusid;
    }
    public void setAsset_old_statusid(int asset_old_statusid) {
        this.asset_old_statusid = asset_old_statusid;
    }

    private int asset_new_statusid;
    public int getAsset_new_statusid() {
        return asset_new_statusid;
    }
    public void setAsset_new_statusid(int asset_new_statusid) {
        this.asset_new_statusid = asset_new_statusid;
    }

    private Date task_start_time;

    public Date getTask_start_time() {
        return task_start_time;
    }

    public void setTask_start_time(Date task_start_time) {
        this.task_start_time = task_start_time;
    }

    private Date task_end_time;
    public Date getTask_end_time() {
        return task_end_time;
    }
    public void setTask_end_time(Date task_end_time) {
        this.task_end_time = task_end_time;
    }

    private int isactive;
    public int getIsactive() {
        return isactive;
    }
    public void setIsactive(int isactive) {
        this.isactive = isactive;
    }

    private String notes;
    public String getNotes() {
        return notes;
    }
    public void setNotes(String notes) {
        this.notes = notes;
    }

    private String created_by;
    public String getCreated_by() {
        return created_by;
    }
    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    private Date create_date;
    public Date getCreate_date() {
        return create_date;
    }
    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    //
//    @JsonManagedReference // It will avoid recursive loop and stack overflow error. Loop -AirlineMaster contains List<Asset> and each Asset contains AirlineMaster
//    @JsonIgnore
//    @OneToMany(mappedBy = "airlineMaster", fetch = FetchType.LAZY) //object name in assetDetail - airlineMaster
//    private List<AssetDetail> assetdetails;

}
