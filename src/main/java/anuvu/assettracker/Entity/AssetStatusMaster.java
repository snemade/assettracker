package anuvu.assettracker.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "asset_status",
        uniqueConstraints=
        @UniqueConstraint(columnNames={"name", "isactive"}))
public class AssetStatusMaster {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name", nullable = false)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    private int isactive;

    public int getIsactive() {
        return isactive;
    }
    public void setIsactive(int isactive) {
        this.isactive = isactive;
    }

    @JsonManagedReference
    // It will avoid recursive loop and stack overflow error. Loop -AirlineMaster contains List<Asset> and each Asset contains AirlineMaster
    @JsonIgnore
    @OneToMany(mappedBy = "assetStatusMaster", fetch = FetchType.LAZY) //object name in assetDetail - assetTypeMaster
    private List<AssetDetail> assetdetails;
}
