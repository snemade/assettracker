package anuvu.assettracker.Entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Entity
public class Asset {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    private String episode_title;

    @JsonBackReference
    @ManyToOne(optional = false)
    @JoinColumn(name = "asset_airline_id", referencedColumnName = "id", foreignKey=@ForeignKey(name = "fk_airline_id"))
    private AirlineMaster airline;


    // Getters and Setters

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getEpisode_title() {
        return episode_title;
    }
    public void setEpisode_title(String episode_title) {
        this.episode_title = episode_title;
    }

    public AirlineMaster getAirline() {
        return airline;
    }
    public void setAirline(AirlineMaster airline) {
        this.airline = airline;
    }

}
