package anuvu.assettracker.Entity;

import javax.persistence.*;

@Entity
@Table(name = "Userdistributors")
public class UserDistributors {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int userdistributorID;

    private String userid;
    private String distributorid;
    private int isactive;

    public int getId() {
        return userdistributorID;
    }

    public void setId(int id) {
        this.userdistributorID = id;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getDistributorid() {
        return distributorid;
    }

    public void setDistributorid(String distributorid) {
        this.distributorid = distributorid;
    }

    public int getIsactive() {
        return isactive;
    }

    public void setIsactive(int isactive) {
        this.isactive = isactive;
    }

    @ManyToOne
    @JoinColumn(name ="FK_Distributor_con")
    private DistributorMaster distributorentity;

    @ManyToOne
    @JoinColumn(name ="FK_User_con")
    private UsersMaster userentity;
}
