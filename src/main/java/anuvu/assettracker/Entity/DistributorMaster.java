package anuvu.assettracker.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "Distributor",
        uniqueConstraints=
        @UniqueConstraint(columnNames={"name", "isactive"}))
public class DistributorMaster {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long distributorid;
    public Long getDistributorid() {
        return distributorid;
    }
    public void setDistributorid(Long distributorid) {
        this.distributorid = distributorid;
    }

    private int isactive;
    public int getIsactive() {
        return isactive;
    }
    public void setIsactive(int isactive) {
        this.isactive = isactive;
    }

    private String name;
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

//    @OneToMany(mappedBy="distributorentity",fetch = FetchType.LAZY)
//    Set<UserDistributors> distributorsfromMap = new HashSet();

    @JsonManagedReference // It will avoid recursive loop and stack overflow error. Loop -AirlineMaster contains List<Asset> and each Asset contains AirlineMaster
    @JsonIgnore
    @OneToMany(mappedBy = "distributorMaster", fetch = FetchType.LAZY ) //object name in assetDetail - distributorMaster
    private List<AssetDetail> assetDetails;

}
