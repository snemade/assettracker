package anuvu.assettracker.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "Language",
        uniqueConstraints=
        @UniqueConstraint(columnNames={"name", "isactive"}))
public class LanguageMaster {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String code;
    private String name;
    private int isActive;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    @JsonManagedReference
    // It will avoid recursive loop and stack overflow error. Loop -AirlineMaster contains List<Asset> and each Asset contains AirlineMaster
    @JsonIgnore
    @OneToMany(mappedBy = "audioLanguageMaster", fetch = FetchType.LAZY) //object name in assetDetail - assetTypeMaster
    private List<AssetDetail> assetdetailsAODLangs;

    @JsonManagedReference
    // It will avoid recursive loop and stack overflow error. Loop -AirlineMaster contains List<Asset> and each Asset contains AirlineMaster
    @JsonIgnore
    @OneToMany(mappedBy = "sub1LanguageMaster", fetch = FetchType.LAZY) //object name in assetDetail - assetTypeMaster
    private List<AssetDetail> assetdetailsSubs1;

    @JsonManagedReference
    // It will avoid recursive loop and stack overflow error. Loop -AirlineMaster contains List<Asset> and each Asset contains AirlineMaster
    @JsonIgnore
    @OneToMany(mappedBy = "sub2LanguageMaster", fetch = FetchType.LAZY) //object name in assetDetail - assetTypeMaster
    private List<AssetDetail> assetdetailsSubs2;
}
