package anuvu.assettracker.Entity;

import javax.persistence.*;

@Entity
@Table(name = "Deliverystatus")
public class DeliveryStatusMaster {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "statusname", nullable = false)
    private String statusname;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatusname() {
        return statusname;
    }

    public void setStatusname(String statusname) {
        this.statusname = statusname;
    }
}
