//https://thorben-janssen.com/jpa-generate-primary-keys/
//@JoinColumn(name = "distributor_id", referencedColumnName = "distributorid", foreignKey=@ForeignKey(name = "fk_distributorid"))
/*
name = Assetdetail[Table].distributor_id - current class table's column in database, if not present JPA will create it.
referencedColumnName = distributor[Table].distributorid - referenced tables column name
foreignKey = foreignKey will be created in current class table with pk in referenced column
 */
package anuvu.assettracker.Entity;

import javax.persistence.*;

@Entity
@Table(name = "AssetDetail")
public class AssetDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "asset_detail_seq_generator")
    @SequenceGenerator(name="asset_detail_seq_generator", sequenceName = "asset_detail_seq", allocationSize=1, initialValue = 1)
    private long transid;

    public long getTransid() {
        return transid;
    }
    public void setTransid(long transid) {
        this.transid = transid;
    }

    private String cycle_id;
    public String getCycle_id() {
        return cycle_id;
    }
    public void setCycle_id(String cycle_id) {
        this.cycle_id = cycle_id;
    }

    @Basic(optional = false)
    private String title;
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    @ManyToOne(optional = false)
    @JoinColumn(name = "distributor_id", referencedColumnName = "distributorid", foreignKey=@ForeignKey(name = "fk_distributorid"))
    private DistributorMaster distributorMaster;

    public DistributorMaster getDistributorMaster() {
        return distributorMaster;
    }
    public void setDistributorMaster(DistributorMaster distributorMaster) {
        this.distributorMaster = distributorMaster;
    }

    // asset status
    @ManyToOne(optional = false)
    @JoinColumn(name = "asset_status_id", referencedColumnName = "id", foreignKey=@ForeignKey(name = "fk_asset_status_id"))
    private AssetStatusMaster assetStatusMaster;

    public AssetStatusMaster getAssetStatusMaster() {
        return assetStatusMaster;
    }
    public void setAssetStatusMaster(AssetStatusMaster assetStatusMaster) {
        this.assetStatusMaster = assetStatusMaster;
    }


    //Airline master
    @ManyToOne(optional = true)
    @JoinColumn(name = "airline_id", referencedColumnName = "id", foreignKey=@ForeignKey(name = "fk_airlineid"), nullable = true)
    private AirlineMaster airlineMaster;

    public AirlineMaster getAirlineMaster() {
        return airlineMaster;
    }
    public void setAirlineMaster(AirlineMaster airlineMaster) {
        this.airlineMaster = airlineMaster;
    }

    //asset_type_id
    @ManyToOne(optional = true)
    @JoinColumn(name = "asset_type_id", referencedColumnName = "id", foreignKey=@ForeignKey(name = "fk_assettypeid"), nullable = true)
    private AssetTypeMaster assetTypeMaster;

    public AssetTypeMaster getAssetTypeMaster() {
        return assetTypeMaster;
    }
    public void setAssetTypeMaster(AssetTypeMaster assetTypeMaster) {
        this.assetTypeMaster = assetTypeMaster;
    }

    private String order_date;
    public String getOrder_date() {
        return order_date;
    }
    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    private String po_number;
    public String getPo_number() {
        return po_number;
    }
    public void setPo_number(String po_number) {
        this.po_number = po_number;
    }


    private String episode_title;
    public String getEpisode_title() {
        return episode_title;
    }
    public void setEpisode_title(String episode_title) {
        this.episode_title = episode_title;
    }

    private int season_no;
    public int getSeason_no() {
        return season_no;
    }
    public void setSeason_no(int season_no) {
        this.season_no = season_no;
    }

    private int episode_no;
    public int getEpisode_no() {
        return episode_no;
    }
    public void setEpisode_no(int episode_no) {
        this.episode_no = episode_no;
    }

    // version
    @ManyToOne(optional = true)
    @JoinColumn(name = "version_id", referencedColumnName = "id", foreignKey=@ForeignKey(name = "fk_versionid"), nullable = true)
    private VersionMaster versionMaster;

    public VersionMaster getVersionMaster() {
        return versionMaster;
    }
    public void setVersionMaster(VersionMaster versionMaster) {
        this.versionMaster = versionMaster;
    }

    // audio langauge
    @ManyToOne(optional = true)
    @JoinColumn(name = "audio_language_id", referencedColumnName = "id", foreignKey=@ForeignKey(name = "fk_audioLanguageID"), nullable = true)
    private LanguageMaster audioLanguageMaster;
    public LanguageMaster getAudioLanguageMaster() {
        return audioLanguageMaster;
    }
    public void setAudioLanguageMaster(LanguageMaster audioLanguageMaster) {
        this.audioLanguageMaster = audioLanguageMaster;
    }

    // Sub langauge 1
    @ManyToOne(optional = true)
    @JoinColumn(name = "subs1_language_id", referencedColumnName = "id", foreignKey=@ForeignKey(name = "fk_subs1_language_id"), nullable = true)
    private LanguageMaster sub1LanguageMaster;

    public LanguageMaster getSub1LanguageMaster() {
        return sub1LanguageMaster;
    }
    public void setSub1LanguageMaster(LanguageMaster sub1LanguageMaster) {
        this.sub1LanguageMaster = sub1LanguageMaster;
    }

    // Sub langauge 2
    @ManyToOne(optional = true)
    @JoinColumn(name = "subs2_language_id", referencedColumnName = "id", foreignKey=@ForeignKey(name = "fk_subs2_language_id"), nullable = true)
    private LanguageMaster sub2LanguageMaster;

    public LanguageMaster getSub2LanguageMaster() {
        return sub2LanguageMaster;
    }
    public void setSub2LanguageMaster(LanguageMaster sub2LanguageMaster) {
        this.sub2LanguageMaster = sub2LanguageMaster;
    }

    private String notes;
    public String getNotes() {
        return notes;
    }
    public void setNotes(String notes) {
        this.notes = notes;
    }

    private int isactive;
    public int getIsactive() {
        return isactive;
    }
    public void setIsactive(int isactive) {
        this.isactive = isactive;
    }

    private String created_by;
    public String getCreated_by() {
        return created_by;
    }
    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

}
