package anuvu.assettracker.security;

public class JwtProperties {
    public static final String SECRET = "SomeSecretForJWTGeneration";
    public static final int EXPIRATION_TIME = 864_000_000; // 10 days
    //public static final int EXPIRATION_TIME = 180000; // 3 min = 3 * 60 = 180 seconds = 180,000 Milliseconds
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
}
